// navbar transparan

window.addEventListener('scroll', () => {

      const nav = document.querySelector('nav');
      const bgWhite = document.querySelector('.bg-white');
      
      if (window.scrollY > nav.offsetTop) {
        nav.classList.add('pink');
        bgWhite.setAttribute('style', 'background: #0277bd !important');
      } else {
        nav.classList.remove('pink');
        bgWhite.style.backgroundColor = '#0277bd';
      }

    });